﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalImagesHolderOperations : MonoBehaviour
{
    public GameObject refFinalImageHOlder;
    public GameObject DrawnFinalImageHOlder;

    public Transform[] MatchingPos;
    public Vector3[] idlePos;
    //public Vector3 offset;

    //public bool matchMode;

    public float finalMatchValueUi;
    public bool setDefault;
    public GameObject starsHolder;
    public GameObject[] starfills;
    //public Slider
    //public float finalMatchValue;

    // Start is called before the first frame update
    void Start()
    {
        idlePos[0] = refFinalImageHOlder.transform.position;
        idlePos[1] = DrawnFinalImageHOlder.transform.position;

        //MatchingPos[0].position = idlePos[0] + offset ;
        //MatchingPos[1].position = idlePos[1] + offset;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagement.instance.MatchingMode)
        {
            if (setDefault)
            {
                setDefault = false;
            }
            refFinalImageHOlder.transform.position = Vector3.Lerp(refFinalImageHOlder.transform.position, MatchingPos[0].position, Time.deltaTime * 5);
            DrawnFinalImageHOlder.transform.position = Vector3.Lerp(DrawnFinalImageHOlder.transform.position, MatchingPos[1].position, Time.deltaTime * 5);
            starsHolder.transform.localScale = Vector3.Lerp(starsHolder.transform.localScale, Vector3.one, Time.deltaTime * 30);

            if (finalMatchValueUi <UiManage.instance. finalMatchValue)
            {
                finalMatchValueUi += Time.deltaTime *50;
                if(finalMatchValueUi > UiManage.instance.finalMatchValue)
                {
                    finalMatchValueUi = UiManage.instance.finalMatchValue;
                }
                UiManage.instance.MatchPecentage.text = "Match " + (int)finalMatchValueUi + " %";
                UiManage.instance.MatchSlider.value = finalMatchValueUi / 100;

                AppearStars(UiManage.instance.MatchSlider.value);

            }
            //matchMode = true;
            
        }
        else
        {
            if (!setDefault)
            {
                refFinalImageHOlder.transform.position = Vector3.Lerp(refFinalImageHOlder.transform.position, idlePos[0], Time.deltaTime * 10);
                DrawnFinalImageHOlder.transform.position = Vector3.Lerp(DrawnFinalImageHOlder.transform.position, idlePos[1], Time.deltaTime * 10);
                starsHolder.transform.localScale = Vector3.Lerp(starsHolder.transform.localScale, Vector3.zero, Time.deltaTime * 10);
                finalMatchValueUi = 0;

                if(Vector3.Distance(refFinalImageHOlder.transform.position , idlePos[0]) < 0.1f)
                {
                    refFinalImageHOlder.transform.position= idlePos[0];
                    DrawnFinalImageHOlder.transform.position = idlePos[1];
                    starsHolder.transform.localScale =  Vector3.zero;
                    finalMatchValueUi = 0;
                    setDefault = true;

                    for (int i = 0; i < starfills.Length; i++)
                    {
                        starfills[i].transform.localScale = Vector3.zero;
                    }
                }
            }


            
            //matchMode = false;
        }
    }

    void AppearStars(float match)
    {
        if(match > 0.3f)
        {
            starfills[0].transform.localScale = Vector3.Lerp(starfills[0].transform.localScale, Vector3.one, Time.deltaTime * 20);
            UiManage.instance.GotStarsInCurrentLevel = 1;
            if (match > 0.6f)
            {
                starfills[1].transform.localScale = Vector3.Lerp(starfills[1].transform.localScale, Vector3.one, Time.deltaTime * 20);
                UiManage.instance.GotStarsInCurrentLevel = 2;
                if (match > 0.9f)
                {
                    starfills[2].transform.localScale = Vector3.Lerp(starfills[2].transform.localScale, Vector3.one, Time.deltaTime * 20);
                    UiManage.instance.GotStarsInCurrentLevel = 3;
                }
            }
            
        }
    }
}
