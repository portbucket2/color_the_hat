﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ShelfCountsManager : MonoBehaviour
{
    //public GameObject[] shelfUnits;
    public List<GameObject> shelUnitList;
    public float shelfGap;

    public int capsCount;
    public int ShelfCount;
    public int MaxCapsCapacity;

    public GameObject ShelfUnit;
    public GameObject HatToPaint;
    public int capsPerShelf;

    public GameObject shelfUnitsHolder;
    public GameObject capsHolder;

    public List<Transform> capHolderTransforms;
    public List<GameObject> capObjects;
    public int capStoredIndex;

    public List<Material> RackCapmats;
    public Material matLWRp;
    public Texture2D[] RackHatTextures;
    public List<Texture2D> RackHatTexturesLoaded;

    Vector3 shelfUnitPos;

    public HatToPaint HatForPainting;

    public GameObject LastHatOnRack;
    public static ShelfCountsManager instance;
    Texture2D tex;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        capsCount = GameManagement.instance.levelIndexPP;
        
        for (int i = 0; i < capsCount; i++)
        {
            AssignRightTextureTotheRackedHat(i);
        }
        
        OrganizeShelfUnits();

        
        


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            GetNewHatIntoRack();
        }
    }


    public void OrganizeShelfUnits()
    {
        
        capsPerShelf = ShelfUnit.GetComponent<ShelfUnit>().CapHolder.Length;
        float remainder = capsCount % capsPerShelf;
        if(remainder == 0 && capsCount != 0)
        {
            ShelfCount = (int)((float)capsCount / (float)capsPerShelf) ;
        }
        else
        {
            ShelfCount = (int)((float)capsCount / (float)capsPerShelf) + 1;
        }

        ShelfMoving.instance.DefineShelfMoveLimit();

        MaxCapsCapacity = ShelfCount * capsPerShelf;
        shelfUnitPos = Vector3.zero;
        for (int i = 0; i < ShelfCount; i++)
        {
            
            GameObject go = Instantiate(ShelfUnit);
            go.transform.SetParent(shelfUnitsHolder.transform);
            go.transform.localPosition = shelfUnitPos;

            shelUnitList.Add(go);

            shelfUnitPos.x += shelfGap;

            for (int j = 0; j < capsPerShelf; j++)
            {
                
                capHolderTransforms.Add(go.GetComponent<ShelfUnit>().CapHolder[j]);

                if (capObjects.Count < capsCount)
                {
                    GameObject goo = Instantiate(HatToPaint, go.GetComponent<ShelfUnit>().CapHolder[j].position, Quaternion.identity);
                    goo.transform.SetParent(capsHolder.transform);
                    capObjects.Add(goo);
                    LastHatOnRack = goo;
                    Material matt = Instantiate(matLWRp);
                    RackCapmats.Add(matt);

                    Texture2D texLoaded = new Texture2D(256, 256, TextureFormat.RGB24, false);
                    //RackHatTexturesLoaded.Add(texLoaded);

                }
                
            }

        }

        for (int i = 0; i < capObjects.Count; i++)
        {
            //capObjects[i].GetComponent<HatToPaint>().ApplyMatToCanvas(RackCapmats[i], RackHatTextures[i]);
            capObjects[i].GetComponent<HatToPaint>().ApplyMatToCanvas(RackCapmats[i], RackHatTexturesLoaded[i]);
            capObjects[i].GetComponent<HatToPaint>().BasePos = HatForPainting.transform;
            capObjects[i].GetComponent<HatToPaint>().RackPos = capHolderTransforms[i].transform;
        }
       
        shelfUnitsHolder.transform.localPosition = new Vector3 (-shelfGap *(float) ShelfCount , 0, 0);
    }

    public void GetNewHatIntoRack()
    {
        capsCount += 1;

        int capIndex = capsCount - 1;

        if(capsCount > MaxCapsCapacity)
        {
        
            ShelfCount += 1;
            ShelfMoving.instance.DefineShelfMoveLimit();
            MaxCapsCapacity += capsPerShelf;
            GameObject go = Instantiate(ShelfUnit);
            go.transform.SetParent(shelfUnitsHolder.transform);
            go.transform.localPosition = shelfUnitPos;
            shelUnitList.Add(go);

            shelfUnitPos.x += shelfGap;

            for (int i = 0; i < capsPerShelf; i++)
            {
                capHolderTransforms.Add(go.GetComponent<ShelfUnit>().CapHolder[i]);
            }


            
        }

        GameObject goo = Instantiate(HatToPaint, capHolderTransforms[capIndex].position, Quaternion.identity);
        goo.transform.SetParent(capsHolder.transform);
        LastHatOnRack = goo;
        capObjects.Add(goo);

        Material matt = Instantiate(matLWRp);
        RackCapmats.Add(matt);

        Texture2D texLoaded = new Texture2D(256, 256, TextureFormat.RGB24, false);
        //RackHatTexturesLoaded.Add(texLoaded);

        //capObjects[capIndex].GetComponent<HatToPaint>().ApplyMatToCanvas(RackCapmats[capIndex], RackHatTextures[capIndex]);
        capObjects[capIndex].GetComponent<HatToPaint>().ApplyMatToCanvas(RackCapmats[capIndex], RackHatTexturesLoaded[capIndex]);
        Debug.Log(RackHatTexturesLoaded[capIndex].name + "textured");
        capObjects[capIndex].GetComponent<HatToPaint>().BasePos = HatForPainting.transform;
        capObjects[capIndex].GetComponent<HatToPaint>().RackPos = capHolderTransforms[capIndex].transform;
        shelfUnitsHolder.transform.localPosition = new Vector3(-shelfGap * (float)ShelfCount, 0, 0);

        capObjects[capIndex].GetComponent<HatToPaint>().GotStars = GameManagement.instance.LastStarAchieved;

    }

    public void GetPaintedHatToRack()
    {
        LastHatOnRack.GetComponent<HatToPaint>().GotoOrigin();
        //HatForPainting.gameObject.SetActive();
        //HatForPainting.GotoRack(); 
    }

    public void AssignRightTextureTotheRackedHat(int levelIndexx)
    {

        //int levelIndexx = GameManagement.instance.levelIndex;
        //byte[] filedata;
        //string filePath = Application.persistentDataPath + "/../SavedImages/takeSnap"+ levelIndexx+".png";

        byte[] filedata;
        //string filePath =DataPathDirectoryManager.instance.filePath  + "/../SavedImages/takeSnap" + levelIndexx + ".png";
        string filePath = DataPathDirectoryManager.instance.filePath + "/takeSnap" + levelIndexx + ".png";
        if (File.Exists(filePath))
        {
            filedata = File.ReadAllBytes(filePath);
            
            tex = new Texture2D(256, 256, TextureFormat.RGB24, false);
            tex.LoadImage(filedata);
            //RackHatTextures[levelIndexx] = tex;
            tex.Apply();
            //RackHatTexturesLoaded.Add(tex);
            RackHatTexturesLoaded.Add(tex);

            if (RackHatTexturesLoaded.Count > levelIndexx+1)
            {
                RackHatTexturesLoaded.Remove(tex);
                RackHatTexturesLoaded[levelIndexx] = tex;
                Debug.Log("Gotttt");
            }

            //Debug.Log(filePath);
        }
        
    }

    //public void ApplyTexture
}
