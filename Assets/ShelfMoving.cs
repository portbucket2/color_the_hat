﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelfMoving : MonoBehaviour
{
    public float SideVelocity;
    public static ShelfMoving instance;

    public bool ShelfSwiped;

    public Vector3 oldShelfPos;
    public Vector3 newShelfPos;

    public Vector3 dummyShelfPos;

    public float moveSpeedMulti;

    public float moveLimitMax;
    public float moveLimitMin;

    public bool withinLimit;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        dummyShelfPos = transform.localPosition;
        oldShelfPos = dummyShelfPos;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagement.instance.GameStartMode)
        {
            ManageSideMovement();
        }
        
    }


    public void ManageSideMovement()
    {

        //this.transform.Translate(SideVelocity* Time.deltaTime * 10, 0, 0);

        
        if (ShelfSwiped)
        {
            //if (SideVelocity > 0.01)
            //{
            //    SideVelocity -= Time.deltaTime*5;
            //}
            //else if (SideVelocity < -0.01)
            //{
            //    SideVelocity += Time.deltaTime*5;
            //}
            //else
            //{
            //    SideVelocity = 0;
            //}
            SideVelocity = Mathf.Lerp(SideVelocity , 0 , Time.deltaTime * 5);
            oldShelfPos = dummyShelfPos;
        }
        dummyShelfPos += new Vector3(SideVelocity * Time.deltaTime * 10, 0, 0);
        dummyShelfPos.x = Mathf.Clamp(dummyShelfPos.x, moveLimitMin, moveLimitMax);

        this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, dummyShelfPos, Time.deltaTime * 20);


    }

    public void GetShelfSideVelocity(Vector3 SwipeVel)
    {
        if (GameManagement.instance.GameStartMode)
        {
            ShelfSwiped = true;
            ShelfMoving.instance.SideVelocity = -SwipeVel.x * 10* moveSpeedMulti;
        }
    }

    public void ShelfTouchGrabbing( Vector3 progression)
    {
        if (GameManagement.instance.GameStartMode)
        {
            ShelfSwiped = false;
            newShelfPos = oldShelfPos + new Vector3(progression.x * 10* moveSpeedMulti, 0, 0);
            dummyShelfPos = newShelfPos;
        }
        
    }

    public void DefineShelfMoveLimit()
    {
        moveLimitMax = ShelfCountsManager.instance.ShelfCount * ShelfCountsManager.instance.shelfGap - (ShelfCountsManager.instance.shelfGap*0.5f);
        moveLimitMin = 0;
    }
}
