﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MaskStencilSet :ScriptableObject
{
    public int Index;
    public GameObject[] maskMeshes;
    public Texture2D[] masktextures;

    public Texture2D finalResultTex;

    public Texture2D[] premadeFilledTextures;

    public int DefaultColorIndex;
    // Start is called before the first frame update
    //void Start()
    //{
    //    
    //}
    //
    //// Update is called once per frame
    //void Update()
    //{
    //    
    //}
}
