﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHandPanel : MonoBehaviour
{
    public float TriggerAnimTimer;
    public float maxTriggerTimer;
    public bool handTutorialOn;
    public GameObject handTutAnimHolder;
    // Start is called before the first frame update
    void Start()
    {
        ResetTutorialTimer();
    }

    // Update is called once per frame
    void Update()
    {

        TriggerHandTutorialAnim();
    }

    public void TriggerHandTutorialAnim()
    {

        if (!Input.GetMouseButton(0)&& GameManagement.instance.PaintMode)
        {
            
            if (!handTutorialOn)
            {
                if (TriggerAnimTimer > maxTriggerTimer )
                {
                    handTutAnimHolder.SetActive(true);
                    handTutorialOn = true;
                }
                else
                {
                    TriggerAnimTimer += Time.deltaTime;
                }
            }
            
        }
        else
        {
            if (handTutorialOn)
            {
                
                handTutorialOn = false;
            }
            ResetTutorialTimer();
        }
        
    }

    public void ResetTutorialTimer()
    {
        TriggerAnimTimer = 0;
        handTutAnimHolder.SetActive(false);
    }
}
