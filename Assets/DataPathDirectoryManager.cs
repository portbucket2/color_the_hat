﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DataPathDirectoryManager : MonoBehaviour
{
    public byte[] filedata;
    public string filePath;
    public static DataPathDirectoryManager instance;
    public Text directoryText;
    void Awake()
    {
        instance = this;
        
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            //string path = Application.persistentDataPath.Substring(0, Application.persistentDataPath.Length - 5);
            //path = path.Substring(0, path.LastIndexOf('/'));
            //filePath = path + "/Documents";

            //filePath = Application.persistentDataPath.Substring(0, Application.persistentDataPath.Length - 5);
            //filePath = Application.persistentDataPath.Substring(0, Application.persistentDataPath.Length - 5);
            filePath = Application.persistentDataPath;

        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            
            filePath = Application.persistentDataPath;
            
        }
        else
        {
            filePath = Application.persistentDataPath;
        }

        //Debug.Log(filePath);
        directoryText.text = filePath;

    }
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(filePath);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
