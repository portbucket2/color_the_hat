﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    public Text completion;
    public Text MatchPecentage;
    public Text coinText;
    public int completionValue;
    public static UiManage instance;
    public Slider fillSlider;
    public Slider MatchSlider;

    public Button nextButton;
    public Button StartGameButton;
    public Button doneGlitterButton;

    bool GotNextButton;

    public Image FinalImage;
    public Image FinalImageDrawn;
    public GameObject GameStartPanel;
    public GameObject GamePaintPanel;
    public GameObject MatchPanel;

    public GameObject glitterPartsOfPaintMode;
    public GameObject nonGlitterPartsOfPaintMode;

    public bool AtUi;
    public float finalMatchValue;
    public int GotStarsInCurrentLevel;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        UpdateCoinsBy(0);
        nextButton.onClick.AddListener(NextButtonFunction);
        doneGlitterButton.onClick.AddListener(NextButtonFunction);
        StartGameButton.onClick.AddListener(GameManagement.instance.GetPaintMode);
        StartGameButton.onClick.AddListener(MaskManager.instance.AppearStencil);
        StartGameButton.onClick.AddListener(GameManagement.instance.GameStartLogForFacebook);


    }

    // Update is called once per frame
    void Update()
    {
        completionValue =100 - MatchmakingSystem.instance.RunTimeFillPercentage;
        completion.text = "" + completionValue +" %";
        fillSlider.value = (float)completionValue / 100;
        //if (GotNextButton)
        //{
        //    nextButton.gameObject.SetActive(true);
        //    GotNextButton = false;
        //}

        if (completionValue >= 95)
        {
            nextButton.gameObject.SetActive(true);
            //GotNextButton = false;
        }
        else
        {
            
            nextButton.gameObject.SetActive(false);
        }
        //else
        //{
        //    nextButton.gameObject.SetActive(false);
        //    GotNextButton = false;
        //}
    }

    void NextButtonFunction()
    {
        if (!SprayParticleHolder.instance.GlitterMode)
        {
            SnapTaker.instance.TakeSnap();
            MaskManager.instance.UpdownStencil();
        }
        else
        {
            MaskManager.instance.NextMask();
            //Debug.Log("nxt");
        }
        

        //nextButton.gameObject.SetActive(false);
        //MaskManager.
    }
    public void GetGameStartPanel()
    {
        GameStartPanel.SetActive(true);
        GamePaintPanel.SetActive(false);
        MatchPanel.SetActive(false);
    }
    public void GetGamePanel()
    {
        GameStartPanel.SetActive(false);
        GamePaintPanel.SetActive(true);
        MatchPanel.SetActive(false);
        LeaveGlitterSubModeUi();
    }
    public void GetMatchPanel()
    {
        GameStartPanel.SetActive(false);
        GamePaintPanel.SetActive(false);
        MatchPanel.SetActive(true);
    }

    

    

    public void UpdateMatchPercentage(int matchValue)
    {
        //MatchPecentage.text = "Match " + matchValue + " %";
        finalMatchValue = matchValue;
    }

    public void ResetUi()
    {
        //GetGamePanel();
    }

    public void GetGlitterSubModeUi()
    {
        glitterPartsOfPaintMode.SetActive(true);
        nonGlitterPartsOfPaintMode.SetActive(false);

        GlitterButtonAppearManage.instance.GlitterModeButtonDisAppear();
    }
    public void LeaveGlitterSubModeUi()
    {
        glitterPartsOfPaintMode.SetActive   (false);
        nonGlitterPartsOfPaintMode.SetActive(true);
    }

    public void AtUiClickBlocking()
    {
        AtUi = true;
    }
    public void AtUiClickEnabling()
    {
        AtUi = false;
    }


    public void FinalMatchingUiOperations()
    {

    }

    public void UpdateCoinsBy(int coinIncrease)
    {
        GameManagement.instance.coins += coinIncrease;
        coinText.text = "" + GameManagement.instance.coins;
        GameManagement.instance.SetPlayerPrefs();
    }

    public void UpdateCoinsByStars()
    {
        GameManagement.instance.LastStarAchieved = GotStarsInCurrentLevel;
        GameManagement.instance.coins += GotStarsInCurrentLevel * 20;
        coinText.text = "" + GameManagement.instance.coins;
        
        GameManagement.instance.SetPlayerPrefs();

        GotStarsInCurrentLevel = 0;
    }
}
