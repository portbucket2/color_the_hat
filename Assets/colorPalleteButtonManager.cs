﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class colorPalleteButtonManager : MonoBehaviour
{
    public GameObject colorPalleteHolder;

    public bool palleteOn;

    public Button[] colorButtons;
    public GameObject backHighlight;

    public Button SelectedColorButton;

    public static colorPalleteButtonManager instance;
    void Awake()
    {
        colorPalleteHolder.SetActive(true);
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(ColorPalleteClickMe);
        //colorPalleteHolder.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ColorPalleteClickMe()
    {
        if (!palleteOn)
        {
            colorPalleteHolder.SetActive(true);
            palleteOn = true;

            UiManage.instance.AtUiClickBlocking();
        }
        else
        {
            colorPalleteHolder.SetActive(false);
            palleteOn = false;

            UiManage.instance.AtUiClickEnabling();
        }
        
    }

    public void ManageColorButtonHighlight(Button but)
    {
        backHighlight.transform.position = but.transform.position;
    }

    public void LoadDefaultColor( int defaultColorIndex)
    {
        colorButtons[defaultColorIndex].GetComponent<ButtonColorPalette>().ThisIsDefaultColor();
    }
}
