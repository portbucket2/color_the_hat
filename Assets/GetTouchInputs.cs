﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetTouchInputs : MonoBehaviour
{
    public bool touched;
    public static GetTouchInputs instance;
    public Vector3 mouseTouchPosition;
    public Text touchText;

    public bool clicked;
    public bool DownClicked;
    public bool OnClickArea;

    public bool SwipedRight;
    public bool SwipedLeft;

    public bool mouseDown;

    public Vector3 clickInPos;
    public Vector3 clickPos;
    public Vector3 clickOutPos;

    public Vector3 ClickFollowerPos;
    public Vector3 SwipeVelocity;
    public Vector3 SwipeProgression;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.touchCount==1)
        //{
        //    //Touch touch = Input.GetTouch(0);
        //    //if (Input.GetTouch(0).phase == TouchPhase.Stationary)
        //    //{
        //    touched = true;
        //    touchText.text = "" + 1;
        //    //    
        //    //}
        //
        //    mouseTouchPosition = Input.GetTouch(0).position;
        //}
        //else
        //{
        //    touched = false;
        //    touchText.text = "" + 0;
        //}

        touchText.text = "progression " + SwipeProgression.x;
        //touchText.text = Application.persistentDataPath;

        //if (Input.GetMouseButtonDown(0))
        //{
        //    ClickFollowerPos= mouseTouchPosition;
        //    clickPos= mouseTouchPosition;
        //    clickInPos = mouseTouchPosition;
        //    
        //    //clickInPos = ClickFollowerPos;
        //    SwipeVelocity = Vector3.zero;
        //    SwipeProgression = Vector2.zero;
        //}
        mouseTouchPosition = Input.mousePosition;
        if (Input.GetMouseButton(0))
        {
            if (!mouseDown)
            {
                ClickFollowerPos = mouseTouchPosition;
                clickPos = mouseTouchPosition;
                clickInPos = mouseTouchPosition;

                //clickInPos = ClickFollowerPos;
                SwipeVelocity = Vector3.zero;
                SwipeProgression = Vector2.zero;
                mouseDown = true;
            }
            else
            {
                clickPos = mouseTouchPosition;
                SwipeProgression = (clickPos - clickInPos) / Screen.width;

                ShelfMoving.instance.ShelfTouchGrabbing(SwipeProgression);
            }
            
        }
        if (Input.GetMouseButtonUp(0))
        {
            clickOutPos = mouseTouchPosition;
            SwipeVelocity = (ClickFollowerPos - clickPos) / Screen.width;
            ShelfMoving.instance.GetShelfSideVelocity(SwipeVelocity);
            mouseDown = false;
        }
        
        ClickFollowerPos = Vector3.Lerp(ClickFollowerPos, clickPos, Time.deltaTime * 10);





        if (OnClickArea && DownClicked)
        {
            clicked = true;

            //clickPos = mouseTouchPosition;
            //SwipeProgression = (clickPos - clickInPos)/ Screen.width;
            //ShelfMoving.instance.ShelfTouchGrabbing(SwipeProgression);
        }
        else
        {
            clicked = false;
            
        }

        
        


    }

    public void OnTheClick()
    {
        
        OnClickArea = true;

        
    }
    public void LeaveClick()
    {
        OnClickArea = false;
    }

    public void OnClickDown()
    {
        DownClicked = true;

        //ClickFollowerPos = mouseTouchPosition;
        //clickInPos = ClickFollowerPos;
        //SwipeVelocity = Vector3.zero;
    }
    public void OnClickUp()
    {
        DownClicked = false;

        //clickOutPos = mouseTouchPosition;
        //SwipeVelocity = (ClickFollowerPos - clickPos) / Screen.width;
        //ShelfMoving.instance.GetShelfSideVelocity(SwipeVelocity);
    }
}
