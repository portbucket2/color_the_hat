﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(SpriteRenderer))]
public class SpriteMaker : MonoBehaviour
{
    
    SpriteRenderer rend;
    public Texture2D[] layers;
    //public Texture2D mask;

    public Texture2D newTex;

    public Color[] layerColors;

    public GameObject matGo;
    public Material mat;
    public Material maskMat;
    public Material maskMat2;
    public Material maskMatUnlit;

    public Material maskMatUnlit2;

    public Material TransparentLayerMat;

    public Texture2D tex;
    Texture2D maskedtex;
    

    public float brushScale;
    public int brushWidth;
    public float brushFlow;
    int brushPixels;
    public Vector2 brushPos;
    public Vector2 brushPosFraction;
    public Vector2 brushPosFractionApp;

    public Vector2 mousePos;
    Color[] colorArray;
    Color[] maskedColorArray;


    Color[][] srcArray;

    public Color[] brushArray;
    Color[] brushSourceArray;
    Color[] maskPixelArray;
    public static SpriteMaker instance;

    

    public bool Snapped;

    public Color BrushColor;
    //public Color[] Colors;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        TexturePreProcess();
        PaintWhiteBoard();

        PaintWhiteBoardToMaskedColor();

        //PaintNewTexture();
        //MakeTexture();

        //MakeSprite();
        //brushArray = new Color[brushPixels *2][];

        //Debug.Log(brushArray.Length);



    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Snapped = false;

            //MakeTexture();
            //MakeSprite();
        }
        if (RaycastPainter.instance.PaintingOn && !SprayParticleHolder.instance.GlitterMode)
        {
            MakeTexture();
            MakeSprite();
        }
        brushPixels = brushWidth / 2;
        //mousePos = Input.mousePosition;

        mousePos = GetTouchInputs.instance.mouseTouchPosition;

        if (Input.GetKeyDown(KeyCode.W))
        {
            PaintWhiteBoard();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            PaintWhiteBoardToMaskedColor();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            PaintNewTexture();
        }
    }

    void TexturePreProcess()
    {
        tex = new Texture2D(layers[0].width, layers[0].height);
        maskedtex = new Texture2D(layers[0].width, layers[0].height);
        colorArray = new Color[tex.width * tex.height];

        maskedColorArray = new Color[tex.width * tex.height];

        //maskPixelArray = new Color[mask.width * mask.height];

        //Debug.Log(tex.width + " " + mask.width);

        srcArray = new Color[layers.Length][];
        for (int i = 0; i < layers.Length; i++)
        {
            srcArray[i] = layers[i].GetPixels();
        }

        brushSourceArray = srcArray[0];

        //maskPixelArray= mask.GetPixels();

    }

    void MakeTexture()
    {

        //brushPos = mousePos;
        //brushPosFraction.x = brushPos.x / tex.width;
        //brushPosFraction.y = brushPos.y / tex.height;
        if(!Snapped)
        {
            brushPosFractionApp = brushPosFraction;
            Snapped = true;
        }
        brushPosFractionApp = Vector2.Lerp(brushPosFractionApp, brushPosFraction, Time.deltaTime *10);

        //brushPosFractionApp = brushPosFraction;

        brushPos.x  = brushPosFractionApp.x* tex.width;
        brushPos.y  = brushPosFractionApp.y* tex.height;


        Vector2 brushInt = new Vector2((int)brushPos.x, (int)brushPos.y);
        ResizeBrushTex();

        

        //for (int x = 0; x < tex.width; x++)
        //{
        //    for (int y = 0; y < tex.height; y++)
        //    {
        //        int pixelIndex = x + (y * tex.width);
        //        for (int i = 0; i < layers.Length; i++)
        //        {
        //            
        //            Color srcPixel = srcArray[i][pixelIndex];
        //            if (srcPixel.a >= 0 && i>0)
        //            {
        //                //colorArray[pixelIndex] = NormalBlend(colorArray[pixelIndex], srcPixel);
        //
        //                
        //            }
        //            else
        //            {
        //                //colorArray[pixelIndex] = srcPixel;
        //            }
        //
        //            if(x < ((brushPosFraction.x * tex.width) + brushPixels) && x >= ((brushPosFraction.x * tex.width) - brushPixels) && y < ((brushPosFraction.y * tex.height) + brushPixels) && y >= ((brushPosFraction.y * tex.height) - brushPixels))
        //            {
        //
        //                //Color[][] brushArray = new Color[][];
        //                if (x < ((brushPosFraction.x * tex.width)))
        //                {
        //                    colorArray[pixelIndex] = Color.green;
        //                    //Debug.Log("FragX" + x);
        //                }
        //                else
        //                {
        //                    colorArray[pixelIndex] = Color.blue;
        //                }
        //                //Debug.Log("X "+ x + "Y " + y);
        //            }
        //            
        //        }
        //
        //        
        //
        //        
        //
        //
        //    }
        //    
        //}
        for (int j = 0; j < brushPixels * 2; j++)
        {
            int pixelIndexx = (int)(brushInt.x + ((brushInt.y - brushPixels + 1 + j) * tex.width));
            for (int i = 0; i < brushPixels * 2; i++)
            {
                if((pixelIndexx - brushPixels + 1 + i) < colorArray.Length && (pixelIndexx - brushPixels + 1 + i) >= 0)
                {
                    //colorArray[pixelIndexx - brushPixels + 1 + i] = Color.green;
                    //colorArray[pixelIndexx - brushPixels + 1 + i] = brushArray[i + (j * brushPixels * 2)];
                    //Color maskPixel = maskPixelArray[pixelIndexx - brushPixels + 1 + i];
                    Color srcPixel = colorArray[pixelIndexx - brushPixels + 1 + i];
                    Color newPixel = brushArray[i + (j * brushPixels * 2)];
                    if (newPixel.a >0f )
                    {
                        colorArray[pixelIndexx - brushPixels + 1 + i] = NormalBlend(colorArray[pixelIndexx - brushPixels + 1 + i], brushArray[i + (j * brushPixels * 2)]);

                        maskedColorArray[pixelIndexx - brushPixels + 1 + i] = NormalBlend(maskedColorArray[pixelIndexx - brushPixels + 1 + i], brushArray[i + (j * brushPixels * 2)]);

                        //if ( maskPixel.a == 0.9f)
                        //{
                        //    maskedColorArray[pixelIndexx - brushPixels + 1 + i] = NormalBlend(maskedColorArray[pixelIndexx - brushPixels + 1 + i], brushArray[i + (j * brushPixels * 2)]);
                        //}
                        //else
                        //{
                        //    maskedColorArray[pixelIndexx - brushPixels + 1 + i] = NormalBlend(colorArray[pixelIndexx - brushPixels + 1 + i], brushArray[i + (j * brushPixels * 2)]);
                        //}
                    }
                    //if (newPixel.a > 0.2f && maskPixel.a >= 0.9f)
                    //{
                    //    colorArray[pixelIndexx - brushPixels + 1 + i] = NormalBlend(colorArray[pixelIndexx - brushPixels + 1 + i], brushArray[i + (j * brushPixels * 2)]);
                    //}

                    //Debug.Log("Index "+ (pixelIndexx - brushPixels + 1 + i) );
                    //int debug = (int)(brushInt.x + ((brushInt.y ) * tex.width));
                    //Debug.Log("brush " + debug );
                }
                
            }
        }
        
        
        //Debug.Log("Stl " + brushInt);
        srcArray[0] = colorArray;
        tex.SetPixels(colorArray);
        tex.Apply();

        tex.wrapMode = TextureWrapMode.Clamp;
        tex.filterMode = FilterMode.Trilinear;

        maskedtex.SetPixels(maskedColorArray);
        maskedtex.Apply();
        
        maskedtex.wrapMode = TextureWrapMode.Clamp;
        maskedtex.filterMode = FilterMode.Trilinear;
    }


    public void MakeSprite()
    {
        Sprite newSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one * 0.5f);

        rend.sprite = newSprite;
        //matGo.GetComponent<Renderer>().material.SetTexture("_MainTex", tex);
        //mat.SetTexture("_MainTex", tex);
        mat.SetTexture("Texture2D_735AD278", tex);
        maskMat.SetTexture("Texture2D_735AD278", maskedtex);

        //maskMat2.SetTexture("Texture2D_735AD278", maskedtex);
        TransparentLayerMat.SetTexture("_BaseMap", tex);

        maskMatUnlit.SetTexture("_MainTex", tex);

        maskMatUnlit2.SetTexture("Texture2D_C47F52E0", tex);

        //maskMat2.SetTexture("Texture2D_EBD9411", maskedtex);
    }

    Color NormalBlend(Color src, Color dest)
    {
        Color blendedColor = new Color();
        if (dest.a < 1)
        {
            
            float destAlpha = dest.a;
            float srcAlpha = (1 - destAlpha) * src.a;
            Color destLayer = dest * destAlpha;
            Color srcLayer = src * srcAlpha;
            
            float Alpha = dest.a;
            
            blendedColor = srcLayer + destLayer;

            blendedColor.a = 1;

            //trying edit

            //destAlpha = dest.a;
            //srcAlpha = src.a;
            //
            //blendedColor.a = Mathf.Lerp(src.a, dest.a, 0.5f);




        }
        else
        {
            blendedColor = dest;
            blendedColor.a = 1;
        }







        return blendedColor;
    }

    void ResizeBrushTex()
    {
        float scalefac =((float)tex.width/((float)brushPixels * 2) );
        //Debug.Log("Scale " + scalefac);

        brushArray = new Color[(brushPixels * 2) * (brushPixels * 2)];
        //Debug.Log("brushArr  " + brushArray.Length);
        
        
        for (int i = 0; i < brushWidth; i++)
        {
            for (int j = 0; j < brushWidth; j++)
            {
                //brushArray[i + (j * brushWidth)] = brushSourceArray[(i*(int)scalefac) + ((j * (int)scalefac * (int)scalefac) * brushWidth)];
                //Debug.Log(i + (j * brushWidth));
                brushArray[(int) (i + (j * brushWidth))] = BrushColor;
                float Alpha = brushSourceArray[(i * (int)scalefac) + ((j * (int)scalefac * (int)scalefac) * brushWidth)].a;

                //brushArray[i + (j * brushWidth)].a = Alpha * Alpha * 0.2f;

                //float a = Mathf.Clamp(((float)i / (float)(brushWidth)* ((float)j / (float)(brushWidth))) , 0.0f,1f);

                float a = ((float)i - (float)((brushWidth - 1) / 2)) / ((float)((brushWidth - 1) / 2));
                float b = ((float)j - (float)((brushWidth - 1) / 2)) / ((float)((brushWidth - 1) / 2));
                
                brushArray[i + (j * brushWidth)] = BrushColor;
                float Radius = Mathf.Sqrt((a*a) + (b*b));

                //Debug.Log("Rad " + Radius);
                //Debug.Log("A " + a +"B "+ b);
                Radius = Mathf.Clamp(Radius , 0 , 1);
                //brushArray[i + (j * brushWidth)].a =(1- Mathf.Abs(a))* (1 - Mathf.Abs(b))* (1 - Mathf.Abs(a)) * (1 - Mathf.Abs(b)) * 0.1f;

                //if(Radius>= (brushWidth ) / 2)
                //{
                //    Radius = (brushWidth - 1) / 2;
                //}
                

                //Alphaa = Mathf.Clamp(Alphaa, 0, 1);
                brushArray[i + (j * brushWidth)].a = ( 1 - Radius) * (1 - Radius) * (1 - Radius) * brushFlow;


                //brushArray[i + (j * brushWidth)].a = (1 - Radius) * (1 - Radius) * 0.1f;

            }
        }

    }

    void PaintWhiteBoard()
    {
        for (int i = 0; i < colorArray.Length; i++)
        {
            colorArray[i] = Color.white;
            //colorArray[i].a = 0;

        }
        //maskedColorArray = colorArray;
        tex.SetPixels(colorArray);
        tex.Apply();

        MakeSprite();

        SpriteMaker.instance.mat.SetTexture("Texture2D_A072C8F", tex);
        SpriteMaker.instance.maskMatUnlit2.SetTexture("Texture2D_AC0BEBC3", tex);
    }

    public void PaintWhiteBoardToMaskedColor()
    {
        for (int i = 0; i < colorArray.Length; i++)
        {
            maskedColorArray[i] = Color.white;
            //colorArray[i].a = 0;
            //maskedColorArray[i] = Color.black;
        }
        //maskedColorArray = colorArray;

        
        maskedtex.SetPixels(maskedColorArray);
        maskedtex.Apply();
        
        MakeSprite();
    }

    public void PaintNewTexture()
    {
        for (int i = 0; i < colorArray.Length; i++)
        {
            //colorArray[i] = newTex.GetPixels();
        
            colorArray[i] = Color.white;
            //colorArray[i] = Color.black;

        }
        //colorArray = newTex.GetPixels();

        

        //mat.SetTexture("Texture2D_A072C8F", tex);
        //maskedColorArray = colorArray;
        tex.SetPixels(colorArray);
        tex.Apply();

        MakeSprite();
    }

    public void ResetCanvas()
    {
        PaintWhiteBoardToMaskedColor();
        PaintWhiteBoard();
        PaintNewTexture();
    }

    
}
