﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastPainter : MonoBehaviour
{
    public GameObject canvasMeshGo;
    public Vector3[] canvasVertces;
    public Vector2[] canvasMeshUv;
    public int[] canvasMeshTriangles;
    public Camera cam;

    public List<Vector3> nearVectices = new List<Vector3>();
    public List<Vector2> nearVecticesUv= new List<Vector2>();
    public List<Vector2> nearVecticesUvSelected = new List<Vector2>();
    public Vector3 lowestNearVectex;
    public Vector3 highestNearVectex;
    public Vector3 hitPoint;

    RaycastHit hit;
    public float RaycastBrshSize;

    public GameObject[] indicator;
    public bool RayCasted;

    public Vector2 OutUv;

    public static RaycastPainter instance;

    public int triadIndex;

    public float mouseOffset;
    Vector3 mousePos;
    public bool PaintingOn;


    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        Mesh canvasMesh = canvasMeshGo.GetComponent<MeshFilter>().mesh;
        canvasVertces = new Vector3[canvasMesh.vertices.Length];
        canvasMeshUv = new Vector2[canvasMesh.vertices.Length];
        canvasMeshTriangles = new int[canvasMesh.triangles.Length];

        for (int i = 0; i < canvasMesh.vertices.Length; i++)
        {
            canvasVertces[i] = transform.TransformPoint(canvasMesh.vertices[i]);
        }
        //canvasVertces = transform.InverseTransformVector(canvasMesh.vertices);
        canvasMeshUv = canvasMesh.uv;
        canvasMeshTriangles = canvasMesh.triangles;
    }

    // Update is called once per frame
    void Update()
    {
        Raycasting();
        if (Input.GetMouseButtonDown(0) )
        {
            //Raycasting();
            //DetectTriadVertces();
            
        }

        if (GameManagement.instance.PaintMode)
        {
            if (RayCasted && !UiManage.instance. AtUi && GetTouchInputs.instance.clicked )
            {
                //if (GetTouchInputs.instance.touched)
                //{
                //    //DetectNearVertices();
                //    PaintingOn = true;
                //
                //    DetectTriadVertces();
                //
                //}

                //DetectNearVertices();
                //if ()
                //{
                //    PaintingOn = true;
                //
                //    DetectTriadVertces();
                //}
                PaintingOn = true;

                DetectTriadVertces();

            }                                                                            
            else
            {
                PaintingOn = false;
            }
        }
        

    }

    void Raycasting()
    {
        //mousePos = Input.mousePosition + new Vector3(0, mouseOffset * Screen.width * 0.01f, 0);

        mousePos = GetTouchInputs.instance.mouseTouchPosition + new Vector3(0, mouseOffset * Screen.width * 0.01f, 0);
        Ray ray = cam.ScreenPointToRay(mousePos); // Construct a ray from the current mouse coordinates
        

        if (Physics.Raycast(ray, out hit , 200f))
        {
            //Debug.DrawLine(hit.point,-transform.forward, Color.red);
            RayCasted = true;

            
            //Debug.Log("triss "+ hit.triangleIndex);
            triadIndex = hit.triangleIndex;
            
            //Instantiate (particle, hit.point, transform.rotation); // Create a particle if hit
        }
        else
        {
            RayCasted = false;
        }
    }

    void DetectNearVertices()
    {
        hitPoint = hit.point;
        nearVectices.Clear();
        nearVecticesUv.Clear();
        nearVecticesUvSelected.Clear();
        for (int i = 0; i < canvasVertces.Length; i++)
        {
            if (Mathf.Abs(hitPoint.x - canvasVertces[i].x) < RaycastBrshSize)
            {
                if (Mathf.Abs(hitPoint.y - canvasVertces[i].y) < RaycastBrshSize)
                {
                    nearVectices.Add(canvasVertces[i]);
                    nearVecticesUv.Add(canvasMeshUv[i]);
                    //Instantiate(indicator[0], canvasVertces[i], Quaternion.identity);

                }
            }
        }
        int uvIndexLow = 0;
        int uvIndexHigh = 0;
        Vector2 averageUv= new Vector2();
        Vector2 sumUv = new Vector2();

        if (nearVectices.Count > 0)
        {
            //lowestNearVectex = nearVectices[0];
            //highestNearVectex = nearVectices[0];
            //for (int i = 0; i < nearVectices.Count; i++)
            //{
            //    if (nearVectices[i].x < lowestNearVectex.x || nearVectices[i].y < lowestNearVectex.y)
            //    {
            //        lowestNearVectex = nearVectices[i];
            //        uvIndexLow = i;
            //    }
            //    if (nearVectices[i].x > highestNearVectex.x || nearVectices[i].y > highestNearVectex.y)
            //    {
            //        highestNearVectex = nearVectices[i];
            //        uvIndexHigh = i;
            //    }
            //}

            for (int i = 0; i < nearVectices.Count; i++)
            {
                sumUv += nearVecticesUv[i];
            }

            averageUv = sumUv / nearVectices.Count;
        }
        
        nearVecticesUvSelected.Add(nearVecticesUv[uvIndexLow]);
        nearVecticesUvSelected.Add(nearVecticesUv[uvIndexHigh]);

        Vector2 advancement = new Vector2 (((hitPoint.x - lowestNearVectex.x)/(highestNearVectex.x - lowestNearVectex.x)), ((hitPoint.y - lowestNearVectex.y) / (highestNearVectex.y - lowestNearVectex.y)));
        

        //Instantiate(indicator[0], lowestNearVectex, Quaternion.identity);
        //Instantiate(indicator[0], highestNearVectex, Quaternion.identity);

        //OutUv = (nearVecticesUvSelected[0] + nearVecticesUvSelected[1] ) / 2;

        //OutUv = nearVecticesUvSelected[0] + new Vector2( (nearVecticesUvSelected[1].x - nearVecticesUvSelected[0].x)*advancement.x , (nearVecticesUvSelected[1].y - nearVecticesUvSelected[0].y) * advancement.y);



        //Instantiate(indicator[1],new Vector3( hitPoint.x , hitPoint.y, hitPoint.z), Quaternion.identity);
        //SpriteMaker.instance.brushPosFraction = OutUv;

        SpriteMaker.instance.brushPosFraction = averageUv;

    }

    void DetectTriadVertces()
    {
        hitPoint = hit.point;

        //int IndexA = canvasMeshTriangles[triadIndex * 3];
        //int IndexB = canvasMeshTriangles[triadIndex * 3 + 1];
        //int IndexC = canvasMeshTriangles[triadIndex * 3 + 2];
        //
        //float disA = Vector3.Distance(hitPoint, canvasVertces[IndexA]);
        //float disB = Vector3.Distance(hitPoint, canvasVertces[IndexB]);
        //float disC = Vector3.Distance(hitPoint, canvasVertces[IndexC]);
        //
        //float frA = disA / (disA + disB + disC);
        //float frB = disB / (disA + disB + disC);
        //float frC = disC / (disA + disB + disC);

        //Debug.Log( "FR "+ frA +" "+ frB + " "+frC);
        //
        //
        //Vector3 posA = canvasVertces[canvasMeshTriangles[triadIndex * 3]];
        //Vector3 posB = canvasVertces[canvasMeshTriangles[triadIndex * 3 + 1]];
        //Vector3 posC = canvasVertces[canvasMeshTriangles[triadIndex * 3 + 2]];
        //
        //Instantiate(indicator[0], posA, Quaternion.identity);
        //Instantiate(indicator[0], posB, Quaternion.identity);
        //Instantiate(indicator[0], posC, Quaternion.identity);
        //
        //Instantiate(indicator[1], hitPoint, Quaternion.identity);
        //
        //Debug.Log("hitt " + hitPoint);
        Vector3[] triadVerts = new Vector3[3];
        int[] triadIndexes = new int[3];

        triadIndexes[0] = canvasMeshTriangles[triadIndex * 3];
        triadIndexes[1] = canvasMeshTriangles[triadIndex * 3 + 1];
        triadIndexes[2] = canvasMeshTriangles[triadIndex * 3 + 2];

        triadVerts[0] = canvasVertces[canvasMeshTriangles[triadIndex * 3]];
        triadVerts[1] = canvasVertces[canvasMeshTriangles[triadIndex * 3 + 1]];
        triadVerts[2] = canvasVertces[canvasMeshTriangles[triadIndex * 3 + 2]];

        int LowXIndex   = 0;
        int HighXIndex  = 0;
        int LowYIndex   = 0;
        int HighYIndex = 0;

        

        for (int i = 0; i < triadVerts.Length; i++)
        {
            if(triadVerts[i].x < triadVerts[LowXIndex].x)
            {
                LowXIndex = i;
            }
            if (triadVerts[i].x > triadVerts[HighXIndex].x)
            {
                HighXIndex = i;
            }

            if (triadVerts[i].y < triadVerts[LowYIndex].y)
            {
                LowYIndex = i;
            }
            if (triadVerts[i].y > triadVerts[HighYIndex].y)
            {
                HighYIndex = i;
            }
        }
        //Instantiate(indicator[0], triadVerts[LowYIndex], Quaternion.identity);
        //Instantiate(indicator[0], triadVerts[HighYIndex], Quaternion.identity);

        float frX = (hitPoint.x - triadVerts[LowXIndex].x) / (triadVerts[HighXIndex].x - triadVerts[LowXIndex].x);
        float frY = (hitPoint.y - triadVerts[LowYIndex].y) / (triadVerts[HighYIndex].y - triadVerts[LowYIndex].y);

        //Debug.Log("x " + frX +" y " + frY);

        //frX = (hitPoint.x - canvasVertces[triadIndexes[LowXIndex]].x) / (canvasVertces[triadIndexes[HighXIndex]].x - canvasVertces[triadIndexes[LowXIndex]].x);
        //frY = (hitPoint.y - canvasVertces[triadIndexes[LowYIndex]].y) / (canvasVertces[triadIndexes[HighYIndex]].y - canvasVertces[triadIndexes[LowYIndex]].y);

        //Debug.Log("x " + frX + " y " + frY);

        //Debug.Log("x " + LowXIndex + "x " + HighXIndex + "y " + LowYIndex + "y" + HighYIndex);

        //Vector2 averageUv = ((canvasMeshUv[IndexA] * (1-frA) ) + (canvasMeshUv[IndexB] * (1 - frB) ) + (canvasMeshUv[IndexC] * (1 - frC) )) * 0.5f;
        Vector2 averageUv = new Vector2();
        averageUv.x = canvasMeshUv[triadIndexes[LowXIndex]].x + (frX * (canvasMeshUv[triadIndexes[HighXIndex]].x - canvasMeshUv[triadIndexes[LowXIndex]].x));
        averageUv.y = canvasMeshUv[triadIndexes[LowYIndex]].y + (frY * (canvasMeshUv[triadIndexes[HighYIndex]].y - canvasMeshUv[triadIndexes[LowYIndex]].y));

        //Vector2 averageUv = (canvasMeshUv[IndexA] + canvasMeshUv[IndexB] + canvasMeshUv[IndexC])/ 3; 
        SpriteMaker.instance.brushPosFraction = averageUv;
    }

}
