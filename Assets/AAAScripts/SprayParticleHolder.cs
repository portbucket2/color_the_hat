﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayParticleHolder : MonoBehaviour
{
    public Vector3 HitPoint;
    public GameObject sprayBottleHolder;
    public ParticleSystem sprayParticle;
    public ParticleSystem spraySmokeParticle;
    public bool SprayOn;
    Vector3 bottleIdlePos;
    public Material bottlemat;
    public static SprayParticleHolder instance;

    public bool GlitterMode;
    public ParticleSystem GlitterParticle;

    public GameObject BrushPointer;
    public GameObject camRotPointer;
    public GameObject camHolder;
    Quaternion camRot;
    Quaternion initialCamRot;

    public Color goldenColor;
    
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        bottleIdlePos =  sprayBottleHolder.transform.position;
        initialCamRot = camRotPointer.transform.rotation;

    }

    // Update is called once per frame
    void Update()
    {
        
        

        if (RaycastPainter.instance.PaintingOn)
        {
            if (!SprayOn)
            {
                if (GlitterMode)
                {
                    GlitterParticle.Play();
                    CancelInvoke("PauseGlitterParticle");
                }
                else
                {
                    sprayParticle.Play();
                }
                
                SprayOn = true;
            }

            HitPoint = RaycastPainter.instance.hitPoint;

            sprayBottleHolder.transform.position = Vector3.Lerp(sprayBottleHolder.transform.position, HitPoint, Time.deltaTime * 50);

            sprayBottleHolder.transform.rotation = Quaternion.LookRotation((sprayBottleHolder.transform.position - RaycastPainter.instance.cam.transform.position), Vector3.up);
            BrushPointer.transform.position = sprayBottleHolder.transform.position;
            
            BrushPointer.transform.rotation = Quaternion.LookRotation((BrushPointer.transform.position - RaycastPainter.instance.cam.transform.position), Vector3.up);

            CamRotPointerRot();

        }
        else
        {
            if (SprayOn)
            {
                sprayParticle.Stop();
                GlitterParticle.Stop();
                SprayOn = false;
                Invoke("PauseGlitterParticle", 2f);
                //PauseGlitterParticle();
            }
            HitPoint = bottleIdlePos;
            sprayBottleHolder.transform.position = Vector3.Lerp(sprayBottleHolder.transform.position, HitPoint, Time.deltaTime * 5);

            

            BrushPointer.transform.position = new Vector3(0, 50, -10);

            camRot = initialCamRot;
        }
        if (!GameManagement.instance.GameStartMode)
        {
            camHolder.transform.rotation = Quaternion.Lerp(camHolder.transform.rotation, camRot, Time.deltaTime * 5f);
        }
        else
        {
            camHolder.transform.localRotation = Quaternion.Euler(new Vector3(0,0,0));
        }
    }

    public void ChangeColor(Color col)
    {
        bottlemat.color = col;
        sprayParticle.startColor = col;
        spraySmokeParticle.startColor = col;
    }

    public void CamRotPointerRot()
    {
        camRotPointer.transform.rotation =  Quaternion.LookRotation((camRotPointer.transform.position - HitPoint), Vector3.up);

        Vector3 camRotRef = Quaternion.ToEulerAngles (camRotPointer.transform.rotation)*Mathf.Rad2Deg * 0.3f ;

        camRot = Quaternion.Euler(camRotRef);
    }

    public void GetGlitterMode()
    {
        GlitterMode = true;
        bottlemat.color = goldenColor ;
    }

    public void LeaveGlitterMode()
    {
        GlitterMode = false;
        GlitterParticle.Clear();
        //ChangeColor(goldenColor);
    }

    public void PauseGlitterParticle()
    {
        GlitterParticle.Pause();
    }
}
