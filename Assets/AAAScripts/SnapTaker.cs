﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SnapTaker : MonoBehaviour
{
    public static SnapTaker instance;
    public Camera cam;
    Texture2D snapShot;
    //public Image snapImage;
    //public SpriteRenderer snapImageSprite;
    Sprite snapSprite;
    public RenderTexture rndTex;

    public Material maskQuad;
    public Material maskQuadUnlit;

    public GameObject stenCilQuad;

    int TakeIndex;

    public bool SaveSnap;
    public bool HighRes;
    public bool RackDataSave;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //cam = Camera.main;
        //TakeSnap();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeSnap();
        }
    }

    public void TakeSnap()
    {
        cam.gameObject.SetActive(true);
        cam.Render();

        //RenderTexture currentActiveRT = new RenderTexture(900, 1600, 24);
        RenderTexture currentActiveRT = rndTex;
        cam.targetTexture = currentActiveRT;

        
        cam.Render();
        RenderTexture.active = currentActiveRT;
        Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height,TextureFormat.RGB24, false);
        image.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
        image.Apply();

        //SavingSnap
        if (SaveSnap)
        {
            Texture2D imageToprint = ResizeTexture(image, 32);
            //SaveScreenShot(imageToprint);

            MatchmakingSystem.instance.matchingImage = imageToprint;
            
            //MatchmakingSystem.instance.MatchMakeImages(MatchmakingSystem.instance.refImageRuntime, MatchmakingSystem.instance.matchingImage);
        }

        if (SaveSnap&& HighRes)
        {
            Texture2D imageToprint = ResizeTexture(image, 256);
            SaveScreenShot(imageToprint);

            ShelfCountsManager.instance.AssignRightTextureTotheRackedHat(GameManagement.instance.levelIndexPP);

            //MatchmakingSystem.instance.matchingImage = imageToprint;
            //MatchmakingSystem.instance.MatchMakeImages(MatchmakingSystem.instance.refImage, MatchmakingSystem.instance.matchingImage);
        }



        //Texture2D image128 = new Texture2D(128, 128, TextureFormat.RGB24, false);
        //image128.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
        //image128.Apply();

        //ResizeTexture(image, 2);

        SpriteMaker.instance.newTex = ResizeTexture(image, 128);
        //snapImage.sprite = Sprite.Create(image, new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), new Vector2(0.5f, 0.5f));
        //snapImageSprite.sprite = Sprite.Create(image, new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), new Vector2(0.5f, 0.5f));

        cam.targetTexture = null;
        RenderTexture.active = null;

        //maskQuad.SetTexture("Texture2D_735AD278", image);
        maskQuadUnlit.SetTexture("Texture2D_C47F52E0", image);
        

        cam.gameObject.SetActive(false);

        

        SpriteMaker.instance.mat.SetTexture("Texture2D_A072C8F", image);
        SpriteMaker.instance.maskMatUnlit2.SetTexture("Texture2D_AC0BEBC3", image);

        SpriteMaker.instance.PaintNewTexture();



        Texture2D texture = image;
        Sprite finalSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        UiManage.instance.FinalImageDrawn.sprite = finalSprite;
    }

    void SaveScreenShot(Texture2D Image)
    {
        byte[] bytes = Image.EncodeToPNG();
        string takeName = "camScreenShot" + TakeIndex;
        //string fileName = Application.persistentDataPath + "/ScreenShots/takeSnap.png";

        //var dirPath = DataPathDirectoryManager.instance.filePath + "/../SavedImages/";
        var dirPath = DataPathDirectoryManager.instance.filePath ;
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }

        string fileName = dirPath + "/takeSnap" + GameManagement.instance.levelIndexPP + ".png";
        //string fileName = Application.persistentDataPath + "/takeSnap" + GameManagement.instance.levelIndex + ".png";

        //string fileName = System.IO.Directory.GetCurrentDirectory() + "/takeSnap" + GameManagement.instance.levelIndex + ".png";
        //string fileName = Application.dataPath + "/takeSnap" +GameManagement.instance.levelIndex +".png";
        System.IO.File.WriteAllBytes(fileName, bytes);
        //Debug.Log("snapTaken");
        //Debug.Log(Application.dataPath);
    }

    public void TakeSnapWithoutStenCil()
    {
        Vector3 stencilPos = stenCilQuad.transform.position;
        stenCilQuad.transform.position = stencilPos+ new Vector3(100, 0, 0);

        cam.gameObject.SetActive(true);
        cam.Render();

        //RenderTexture currentActiveRT = new RenderTexture(900, 1600, 24);
        RenderTexture currentActiveRT = rndTex;
        cam.targetTexture = currentActiveRT;


        cam.Render();
        RenderTexture.active = currentActiveRT;
        Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height, TextureFormat.RGB24, false);
        image.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
        image.Apply();

        //Texture2D image128 = new Texture2D(128, 128, TextureFormat.RGB24, false);
        //image128.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
        //image128.Apply();

        //ResizeTexture(image, 2);

        SpriteMaker.instance.newTex = ResizeTexture(image, 128);
        //snapImage.sprite = Sprite.Create(image, new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), new Vector2(0.5f, 0.5f));
        //snapImageSprite.sprite = Sprite.Create(image, new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), new Vector2(0.5f, 0.5f));

        cam.targetTexture = null;
        RenderTexture.active = null;

        //maskQuad.SetTexture("Texture2D_735AD278", image);
        //maskQuadUnlit.SetTexture("Texture2D_C47F52E0", image);


        cam.gameObject.SetActive(false);

        SpriteMaker.instance.PaintNewTexture();

        SpriteMaker.instance.mat.SetTexture("Texture2D_A072C8F", image);
        SpriteMaker.instance.maskMatUnlit2.SetTexture("Texture2D_AC0BEBC3", image);

        stenCilQuad.transform.position = stencilPos;
    }

    public Texture2D ResizeTexture(Texture2D SourceTex , int newRes)
    {
        int SourceRes = SourceTex.width;
        int downScale = (int)(SourceRes / newRes);
        //int newRes = (int)(SourceRes / downScale);
        Texture2D resizedTex =  new Texture2D(newRes , newRes, TextureFormat.RGB24, false);
        Color[] sourceColorArray = new Color[SourceRes * SourceRes];
        Color[] resizedColorArray = new Color[newRes* newRes];

        //Debug.Log("r " + resizedColorArray.Length + " S " + sourceColorArray.Length + " new res "+ newRes );
        sourceColorArray = SourceTex.GetPixels();

        for (int i = 0; i < newRes; i++)
        {
            for (int j = 0; j < newRes; j++)
            {
                resizedColorArray[i + (j * newRes)] = sourceColorArray[(i * downScale) + (j * newRes  * downScale * downScale)];
        
            }
        }

        resizedTex.SetPixels(resizedColorArray);

        resizedTex.Apply();


        return resizedTex;
    }
}
