﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrushVisualWindow : MonoBehaviour
{
    public Image brushVisualImage;
    
    public float brushSize;
    public float brushFlow;

    public Slider BrushSizeSlider;
    public Slider BrushFlowSlider;

    Color BrushColor;

    public int resolution;
    
    // Start is called before the first frame update
    void Start()
    {
        BrushSizeSlider.value= 0.5f;
        BrushFlowSlider.value= 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        BrushSizeAndFlow();
    }

    void BrushSizeAndFlow()
    {
        float multi =  (float)resolution / 256;
        int brushLow = (int)(8 * multi);
        int brushHigh = (int)(36 * multi);
        brushSize =Mathf.Lerp(brushLow, brushHigh,    BrushSizeSlider.value );
        brushFlow = Mathf.Lerp(0.02f, 0.9f, BrushFlowSlider.value);

        float brushVisualScale = Mathf.Lerp(0.2f, 1f, BrushSizeSlider.value);

        brushVisualImage.transform.localScale = new Vector3(brushVisualScale, brushVisualScale, brushVisualScale);
        BrushColor = SpriteMaker.instance.BrushColor;
        BrushColor.a = brushFlow + 0.2f;
        brushVisualImage.color = BrushColor;

        SpriteMaker.instance.brushWidth = (int)brushSize * 2;
        SpriteMaker.instance.brushFlow = brushFlow;
        float pointerScale = BrushSizeSlider.value + 0.2f;
        SprayParticleHolder.instance.BrushPointer.transform.localScale = new Vector3(pointerScale, pointerScale, pointerScale);

    }
}
