﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskManager : MonoBehaviour
{
    public MaskStencilSet[] MaskStencilSets;
    public Texture2D[] masks;
    public Texture2D white;

    

    public int maskIndex;
    public int maxMaskIndex;

    public GameObject stencilHolder;
    Vector3 stencilPos;
    Vector3 stencilDownPos;
    Vector3 stencilUpPos;

    public bool StencilUp;

    public GameObject[] StencilsMeshes;

    public Texture2D[] premadeFills;

    public GameObject Garbages;
    public Material[] maskmats;

    public static MaskManager instance;

    public Animator stencilHolderAnim;

    //public Material finalResultMat;

    //public bool matchState;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

        stencilHolderAnim = stencilHolder.GetComponentInParent<Animator>();
    }
    void Start()
    {
        Garbages = new GameObject();
        StencilPosInitialization();
        MaskInitialization();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            NextMask();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            UpdownStencil();
        }

        //stencilHolder.transform.position = Vector3.Slerp(stencilHolder.transform.position, stencilPos, Time.deltaTime * 5f);
        //stencilHolder.transform.position = Vector3.MoveTowards(stencilHolder.transform.position, stencilPos, Time.deltaTime * 10f);
        if (StencilUp && Vector3.Distance(stencilHolder.transform.position, stencilUpPos) < 0.5f)
        {
            stencilPos = stencilDownPos;

            StencilUp = false;
            NextMask();
            //stencilHolderAnim.SetBool("StencilDown", true);
        }

        //if (Input.GetKeyDown(KeyCode.I))
        //{
        //    ResetMaskLevel();
        //}
    }

    public void NextMask()
    {
        if(maskIndex < maxMaskIndex)
        {
            maskIndex += 1;
            UpdateMask();
            stencilHolderAnim.SetBool("StencilDown", true);
        }
        else if(maskIndex == maxMaskIndex)
        {
            maskIndex += 1;
            SprayParticleHolder.instance.GetGlitterMode();
            UiManage.instance.GetGlitterSubModeUi();
            stencilHolder.SetActive(false);
            MatchmakingSystem.instance.MatchMakeFinally();

            UiManage.instance.UpdateMatchPercentage(MatchmakingSystem.instance.FinalMatchValue);

            //stencilHolderAnim.SetBool("StencilDown", true);
        }
        else
        {
            //maskIndex = 0;
            //matchState = true;
            
            //UiManage.instance.GetMatchPanel();
            GameManagement.instance.GetMatchingMode();

            //MatchmakingSystem.instance.MatchMakeFinally();

            //UiManage.instance.UpdateMatchPercentage(MatchmakingSystem.instance.FinalMatchValue);

            //SprayParticleHolder.instance.LeaveGlitterMode();
            //GetGarbages();
        }
        //UpdateMask();
    }

    public void UpdateMask()
    {
        
        SpriteMaker.instance.PaintWhiteBoardToMaskedColor();

        maskmats[0].SetTexture("Texture2D_6684D012", masks[maskIndex]);
        maskmats[1].SetTexture("Texture2D_6684D012", masks[maskIndex]);

        //maskmats[2].SetTexture("Texture2D_C47F52E0", masks[maskIndex]);

        maskmats[2].SetTexture("Texture2D_930C5BCA", masks[maskIndex]);


        for (int i = 0; i < StencilsMeshes.Length; i++)
        {
            if (i == maskIndex)
            {
                StencilsMeshes[i].SetActive(true);
            }
            else
            {
                StencilsMeshes[i].SetActive(false);
            }
        }

        MatchmakingSystem.instance.UpdateRuntimeMatching(premadeFills[maskIndex]);
    }

    public void UpdownStencil()
    {
        if (!StencilUp)
        {
            
            stencilPos = stencilUpPos;
            StencilUp = true;

            stencilHolderAnim.SetBool("StencilDown", false);

            
        }
        

        
    }

    
    public void RestartGame()
    {
        Application.LoadLevel(0);
    }

    public void MaskInitialization()
    {
        

        GetGarbages();

        colorPalleteButtonManager.instance.LoadDefaultColor(MaskStencilSets[GameManagement.instance.levelIndex].DefaultColorIndex);

        SprayParticleHolder.instance.LeaveGlitterMode();
        //stencilHolder
        StencilsMeshes = new GameObject[MaskStencilSets[GameManagement.instance.levelIndex].maskMeshes.Length];
        masks = new Texture2D[MaskStencilSets[GameManagement.instance.levelIndex].maskMeshes.Length];
        premadeFills = new Texture2D[MaskStencilSets[GameManagement.instance.levelIndex].maskMeshes.Length];

        maxMaskIndex = masks.Length - 1;

        for (int i = 0; i < masks.Length; i++)
        {
            masks[i] = MaskStencilSets[GameManagement.instance.levelIndex].masktextures[i];

            premadeFills[i] = SnapTaker.instance.ResizeTexture(MaskStencilSets[GameManagement.instance.levelIndex].premadeFilledTextures[i], 32);
            //premadeFills[i] = SnapTaker.instance.ResizeTexture(masks[i], 32);

            StencilsMeshes[i] = Instantiate(MaskStencilSets[GameManagement.instance.levelIndex].maskMeshes[i], stencilHolder.transform.position, stencilHolder.transform.rotation);
            StencilsMeshes[i].transform.SetParent(stencilHolder.transform);
        }
        Texture2D texture = MaskStencilSets[GameManagement.instance.levelIndex].finalResultTex;
        Sprite finalSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        UiManage.instance.FinalImage.sprite = finalSprite;

        MatchmakingSystem.instance.refImageFinal = SnapTaker.instance.ResizeTexture(MaskStencilSets[GameManagement.instance.levelIndex].finalResultTex, 32);

        


        maskmats[0].SetTexture("Texture2D_6684D012", masks[maskIndex]);
        maskmats[1].SetTexture("Texture2D_6684D012", masks[maskIndex]);

        maskmats[2].SetTexture("Texture2D_930C5BCA", masks[maskIndex]);

        maskmats[0].SetTexture("Texture2D_735AD278", white);
        maskmats[1].SetTexture("Texture2D_735AD278", white);

        maskmats[2].SetTexture("Texture2D_C47F52E0", white);

        for (int i = 0; i < StencilsMeshes.Length; i++)
        {
            if (i == maskIndex)
            {
                StencilsMeshes[i].SetActive(true);
            }
            else
            {
                StencilsMeshes[i].SetActive(false);
            }
        }
        UpdateMask();
        if (GameManagement.instance.PaintMode)
        {
            //Invoke( "AppearStencil", 0.2f);
        }
        
    }

    public void ResetMaskLevel()
    {
        maskIndex = 0;
        MaskInitialization();
        SpriteMaker.instance.ResetCanvas();
    }

    public void GetGarbages()
    {
        for (int i = 0; i < masks.Length; i++)
        {
            
            StencilsMeshes[i].transform.SetParent(Garbages.transform);
            StencilsMeshes[i].SetActive(false);
            //StencilsMeshes[i] = null;
        }
    }

    public void StencilPosInitialization()
    {
        
        stencilDownPos = Vector3.zero;
        
        stencilUpPos = stencilDownPos + new Vector3(0, 15, -2);
        
        stencilHolder.SetActive(false);

        

        //stencilPos = stencilUpPos;
    }

    public void AppearStencil()
    {
        stencilHolder.SetActive(true);
        stencilHolderAnim.SetBool("StencilDown", true);
        //stencilHolder.transform.position = stencilUpPos;
        //stencilPos = stencilDownPos;
    }


}
