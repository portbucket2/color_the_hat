﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchmakingSystem : MonoBehaviour
{
    public Texture2D refImageRuntime;
    

    public Texture2D runtimeMatchingImage;

    public Texture2D refImageFinal;

    public Texture2D matchingImage;

    public static MatchmakingSystem instance;

    //Color[] runtimeColorArray;
    public List<int> runtimeColorIndexList = new List<int>();

    bool RuntimeListing;

    public int RunTimeFill;
    public int RuntimeMaxFill;
    public int RunTimeFillPercentage;


    public int FinalMatchValue;

    public Text colorFillText;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        //MatchMakeImages(refImage, matchingImage);
        //MatchMakeRuntime(refImage, matchingImage);
    }

    // Update is called once per frame
    void Update()
    {
        runtimeMatchingImage = SnapTaker.instance.ResizeTexture(SpriteMaker.instance.tex, 32);
        if (refImageRuntime)
        {
            RunTimeFillPercentage = MatchMakeRuntime(refImageRuntime, runtimeMatchingImage);
        }
        colorFillText.text = RunTimeFill + " / " + RuntimeMaxFill;
    }

    public void MatchMakeFinally()
    {
        FinalMatchValue =  MatchMakeImages(refImageFinal, matchingImage);
    }


    public int MatchMakeImages(Texture2D reff , Texture2D image)
    {
        int matchPercentage = 0;
        int res = reff.width * reff.width;
        Color[] refColors = new Color[res ];
        Color[] imageColors = new Color[res ];

        
        refColors = reff.GetPixels();
        imageColors = image.GetPixels();
        
        int matchPoint = 0;
        int iterations = 0;
        float negPoint = 0;
        for (int i = 0; i < refColors.Length; i++)
        {
            float H, S, V;
            Color.RGBToHSV(refColors[i], out H, out S, out V);
            float H1, S1, V1;
            Color.RGBToHSV(imageColors[i], out H1, out S1, out V1);
            if(refColors[i] != Color.white)
            {
                //if (Mathf.Abs(S - S1) < 0.1f)
                //{
                //    if (Mathf.Abs(H - H1) < 0.05f)
                //    {
                //        //matchPoint += 1;
                //    }
                //}

                if (imageColors[i] != Color.white)
                {
                    matchPoint += 1;
                    if (Mathf.Abs(H - H1) > 0.1f)
                    {
                        negPoint += 0.25f;
                    }
                }

                iterations += 1;
            }


            

            

            //if (H == H1)
            //{
            //    //Debug.Log("Hue " + H1);
            //}

            
        }
        
        //Debug.Log(matchPoint + " / " + iterations);

        //int matchPer = (int)(((float)matchPoint + negPoint)* 100 / (float)refColors.Length);
        //matchPercentage =(int) (Mathf.Lerp(0, 100, ((float)matchPer - 35) / (100 - 35)));

        matchPercentage = (int)(((float)matchPoint - negPoint) * 100 / (float)iterations);

        if(matchPercentage > 90)
        {
            matchPercentage = 100;
        }
        //Debug.Log(matchPercentage);
        return matchPercentage;
    }

    public int MatchMakeRuntime(Texture2D reff, Texture2D image)
    {
        int matchPercentage = 0;
        int res = reff.width * reff.width;
        Color[] refColors = new Color[res];
        Color[] imageColors = new Color[res];
        refColors = reff.GetPixels();
        imageColors = image.GetPixels();
        if (!RuntimeListing)
        {
            runtimeColorIndexList.Clear();
            for (int i = 0; i < refColors.Length; i++)
            {
                if (refColors[i] != Color.white)
                {
                    runtimeColorIndexList.Add(i);
                }

                //if(refColors[i].a < 0.1f)
                //{
                //    runtimeColorIndexList.Add(i);
                //}
            }
            RuntimeMaxFill = runtimeColorIndexList.Count;
            RuntimeListing = true;
        }

        int fillPoint = runtimeColorIndexList.Count;
        for (int i = 0; i < runtimeColorIndexList.Count; i++)
        {
            if (imageColors[runtimeColorIndexList[i]] != Color.white)
            {
                fillPoint -= 1;
                runtimeColorIndexList.Remove(runtimeColorIndexList[i]);
            }


        }
        RunTimeFill = fillPoint;
        //Debug.Log("fill" + fillPoint);
        
        matchPercentage = (int)((float)fillPoint * 100 / ((float)RuntimeMaxFill  )) ;

        matchPercentage = Mathf.Clamp(matchPercentage, 0, 100);

        return matchPercentage;
    }

    public void UpdateRuntimeMatching(Texture2D image)
    {
        refImageRuntime = image;
        RuntimeListing = false;
    }
}
