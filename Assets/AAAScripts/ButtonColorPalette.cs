﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonColorPalette : MonoBehaviour
{
    public Color myColor;
    public bool Glitter;
    public Material[] ColorMats;

    public bool DefaultColor;
    // Start is called before the first frame update
    void Awake()
    {
        
    }
    void Start()
    {
        myColor = GetComponent<Image>().color;

        if (Glitter)
        {
            //this.GetComponent<Button>().onClick.AddListener(GetGlitterMode);
        }
        else
        {
            this.GetComponent<Button>().onClick.AddListener(AssignColorToBrush);
        }


        if (DefaultColor)
        {
            //AssignColorToBrush();
            ThisIsDefaultColor();
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AssignColorToBrush()
    {
        SpriteMaker.instance.BrushColor = myColor;
        SprayParticleHolder.instance.ChangeColor(myColor);
        SprayParticleHolder.instance.GlitterMode = false;

        ColorMats[0].SetColor("Color_9EE7D228", myColor);
        ColorMats[1].SetColor("Color_AA4D94BB", myColor);

        colorPalleteButtonManager.instance.ManageColorButtonHighlight(this.GetComponent<Button>());
        colorPalleteButtonManager.instance.ColorPalleteClickMe();
    }

    public void GetGlitterMode()
    {
        SprayParticleHolder.instance.GlitterMode = true;
        SprayParticleHolder.instance.ChangeColor(myColor);
    }

    public void ThisIsDefaultColor()
    {
        SpriteMaker.instance.BrushColor = myColor;
        SprayParticleHolder.instance.ChangeColor(myColor);
        SprayParticleHolder.instance.GlitterMode = false;

        ColorMats[0].SetColor("Color_9EE7D228", myColor);
        ColorMats[1].SetColor("Color_AA4D94BB", myColor);

        colorPalleteButtonManager.instance.ManageColorButtonHighlight(this.GetComponent<Button>());
        colorPalleteButtonManager.instance.colorPalleteHolder.SetActive(false);
    }
}
