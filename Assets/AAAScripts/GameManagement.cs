﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class GameManagement : MonoBehaviour
{
    public static GameManagement instance;
    public int levelIndexPP;
    public int levelIndex;

    public int coins;
    

    public int maxLevelIndex;

    public bool GameStartMode;
    public bool PaintMode;
    public bool MatchingMode;

    List<int> Intlist;
    public string StarArrayPP;

    public int LastStarAchieved;

    //enum Mode {GameStartMode , Paintmode , Matchingmode };
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        GetPlayerPrefs();
        maxLevelIndex = MaskManager.instance.MaskStencilSets.Length - 1;
        if (levelIndexPP >= (maxLevelIndex + 1))
        {
            levelIndex = levelIndexPP % (maxLevelIndex + 1);
        }
        else
        {
            levelIndex = levelIndexPP;
        }
        
    }
    void Start()
    {
        //GetGameMode(1);
        GetGameStartMode();

        
        
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextLevel()
    {
        //levelIndex += 1;

        FacebookManager.LogLevelCompleted(levelIndexPP + 1);

        UiManage.instance.UpdateCoinsByStars();
        levelIndexPP += 1;
        if (levelIndex < maxLevelIndex)
        {
            levelIndex += 1;
        }
        else
        {
            levelIndex = 0;
        }

        //UiManage.instance.ResetUi();
        //GameManagement.instance.GetPaintMode();
        GameManagement.instance.GetGameStartMode();
        MaskManager.instance.ResetMaskLevel();

        ShelfCountsManager.instance.GetNewHatIntoRack();
        
        ShelfCountsManager.instance.GetPaintedHatToRack();
        //SetStarPPForHats();
        SetPlayerPrefs();
    }

    public void RestartLevel()
    {
        MaskManager.instance.ResetMaskLevel();
        //UiManage.instance.ResetUi();
        GameManagement.instance.GetPaintMode();
    }

    public void GetGameMode(int modeIndex)
    {
        switch (modeIndex)
        {
            case 0:
                GameStartMode= true ;
                PaintMode    = false ;
                MatchingMode = false;
                break;
            case 1:
                GameStartMode = false;
                PaintMode = true;
                MatchingMode = false;
                break;

            case 2:
                GameStartMode = false;
                PaintMode = false;
                MatchingMode = true;
                break;

        }
    }

    public void GetGameStartMode()
    {
        GetGameMode(0);
        UiManage.instance.GetGameStartPanel();

    }
    public void GetPaintMode()
    {
        GetGameMode(1);
        UiManage.instance.GetGamePanel();
    }
    public void GetMatchingMode()
    {
        GetGameMode(2);
        UiManage.instance.GetMatchPanel();
    }

    public void SetPlayerPrefs()
    {
        PlayerPrefs.SetInt("levelIndexPP", levelIndexPP);
        PlayerPrefs.SetInt("coins", coins);

        
        


    }
    public void GetPlayerPrefs()
    {
        levelIndexPP = PlayerPrefs.GetInt("levelIndexPP", 0);
        coins = PlayerPrefs.GetInt("coins", 0);

        //StarArrayPP = PlayerPrefs.GetString("StarArrayPP","");
    }

    public void GetCapStarValuesFromHistory()
    {
        string str = "1,2,3,4";
        
        

        int mos = 0;
        var intList = str.Split(',')
                    .Where(m => int.TryParse(m, out mos))
                    .Select(m => int.Parse(m))
                    .ToList();
        Intlist = new List<int>();
        for (int i = 0; i < intList.Count; i++)
        {
            Intlist.Add(intList[i]);
        }
    }

    public void SetStarPPForHats()
    {
        StarArrayPP = StarArrayPP + LastStarAchieved + ",";
        PlayerPrefs.SetString("StarArrayPP", StarArrayPP);

        //Debug.Log(StarArrayPP);
    }


    public void GameStartLogForFacebook()
    {
        FacebookManager.LogLevelStarted(levelIndexPP + 1);
    }

}
