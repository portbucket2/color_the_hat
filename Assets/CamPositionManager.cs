﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPositionManager : MonoBehaviour
{
    public Transform rackViewPos;
    public Vector3 capViewPos;
    Vector3 camHolderPos;
    Quaternion camHolderRot;
    Transform RootPosTransform;
    //public GameObject CamHolder;
    // Start is called before the first frame update
    void Start()
    {
        RootPosTransform = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        CamPosManage();
    }

    void CamPosManage()
    {
        if (GameManagement.instance.GameStartMode)
        {
            camHolderPos = rackViewPos.position;
            camHolderRot = rackViewPos.rotation;
        }
        else
        {
            camHolderPos = capViewPos;
            camHolderRot = RootPosTransform.rotation;
        }
        transform.position = Vector3.Lerp(transform.position, camHolderPos, Time.deltaTime * 5);

        transform.rotation = Quaternion.Lerp(transform.rotation, camHolderRot, Time.deltaTime * 5);
    }
}
