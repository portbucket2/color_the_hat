﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class ClickDetectorButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static bool clicked;
    public UnityEvent onLongClick;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Image>().alphaHitTestMinimumThreshold = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        //if(GetComponent<Button>().onClick)
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        clicked = true;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        clicked = false;
    }
    //public void OnPoiterClick(PointerEventData eventData)
    //{
    //
    //}
}
