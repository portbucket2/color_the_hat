﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitterButtonAppearManage : MonoBehaviour
{
    //public bool GlitterMode;
    public float GlitterDoneTimer;
    public float MaxGlitterDoneTime;
    public bool GlitterDone;
    public static GlitterButtonAppearManage instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (GlitterMode )
        //{
        //    if((GlitterDoneTimer > MaxGlitterDoneTime))
        //    {
        //        GlitterDoneTimer = 0;
        //        GlitterMode = false;
        //    }
        //    else
        //    {
        //        GlitterDoneTimer += Time.deltaTime;
        //    }
        //    
        //}

        if (GetTouchInputs.instance.clicked && !GlitterDone)
        {
            GlitterDoneTimer += Time.deltaTime;
            if ((GlitterDoneTimer > MaxGlitterDoneTime))
            {
                GlitterDoneTimer = 0;
                GlitterDone = true;
                UiManage.instance.doneGlitterButton.gameObject.SetActive(true);
            }
        }
    }

    public void GlitterModeButtonDisAppear()
    {
        GlitterDone = false;
        UiManage.instance.doneGlitterButton.gameObject.SetActive(false);
    }
    //public void GlitterModeButtonAppear()
    //{
    //    GlitterDone = false;
    //    UiManage.instance.doneGlitterButton.gameObject.SetActive(false);
    //}


}
