﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatToPaint : MonoBehaviour
{
    public GameObject DrawCanvas;
    public Material canvasMat;

    public Transform BasePos;
    public Transform RackPos;

    public bool AtBase;
    public bool SentToOrigin;
    Transform HatTransform;

    public Material[] hatMats;

    public int GotStars;
    public ParticleSystem coinParticle;

    public bool coroutineStarted;
    // Start is called before the first frame update
    void Start()
    {
        HatTransform = this.transform;

        GotStars = Random.Range(1, 3);

        //if (!AtBase)
        //{
        //    StartCoroutine( CoinGenCoroutine());
        //}
        //BasePos
        //if (AtBase)
        //{
        //    DrawCanvas.GetComponent<Renderer>().material = hatMats[0];
        //}
        //else
        //{
        //    DrawCanvas.GetComponent<Renderer>().material = hatMats[1];
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (!AtBase)
        {
            if (SentToOrigin)
            {
                //transform.position = HatTransform.position;
                HatTransform = RackPos;
                
                
                transform.position = Vector3.Lerp(transform.position, HatTransform.position, Time.deltaTime * 5f);

                if ((Mathf.Abs(transform.position.y - HatTransform.position.y) < 0.1f))
                {
                    SetoRack();
                    transform.position = HatTransform.position;
                    if (!coroutineStarted)
                    {
                        StartCoroutine(CoinGenCoroutine());
                        coroutineStarted = true;
                    }
                }

                //transform.position = Vector3.MoveTowards(transform.position, HatTransform.position, Time.deltaTime * 500f);
            }
            if (!coroutineStarted)
            {
                StartCoroutine(CoinGenCoroutine());
                coroutineStarted = true;
            }
            //else
            //{
            //
            //    //HatTransform = RackPos;
            //    //transform.position = HatTransform.position;
            //}
            //HatTransform = BasePos;

        }
        //else
        //{
        //    //HatTransform = RackPos;
        //}

        
    }

    public void ApplyMatToCanvas(Material mat, Texture2D tex)
    {
        //hatMats[1].SetTexture("Texture2D_A072C8F", tex);
        mat.SetTexture("Texture2D_A072C8F", tex);
        DrawCanvas.GetComponent<Renderer>().material = mat;
        
    }

    public void SetoRack()
    {
        SentToOrigin = false;
    }
    public void GotoOrigin()
    {
        SentToOrigin = true;
        transform.position = BasePos.position;
    }

    public void StartCoinGeneration()
    {
        coinParticle.Play();
        UiManage.instance.UpdateCoinsBy(GotStars);
    }

    IEnumerator CoinGenCoroutine()
    {
        //Print the time of when the function is first called.
        //Debug.Log("Started Coroutine at timestamp : " + Time.time);
        //StartCoinGeneration();
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(1);
        StartCoinGeneration();
        StartCoroutine(CoinGenCoroutine());
        //After we have waited 5 seconds print the time again.
        //Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }
}
